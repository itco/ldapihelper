# Change Log #

## In progress ##

 * ...

## v2.0.1 ##

 * Added test method for login
 * Added test method for changeUserPassword

## v2.0.0 ##

 * Added mock methods for `TestLdapi` to save, update and delete objects
 * Added mock methods for `TestLdapi` to retrieve email forwards
 * Restructure test fixtures - They now need to be included with the `ldap_fixtures` property to make the process more explicit
 * Added meta tests (tests to test the testsuit, i.e. usage examples)
 * Added code coverage and autodocs
 * Increated test coverage and doc blocks
