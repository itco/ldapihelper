<?php

namespace LdapiHelper\TestSuite;

use LdapiHelper\LDAP\LDAPIConnection;
use Cake\Core\Configure;

/**
 * Use local fixtures instead of actual LDAP connection
 *
 * When this trait is included, *no* actual LDAP connection is used. This allows
 * you to use your own users fixtures data instead of actual data, and to run
 * your tests without having access to the LDAP server.
 * Note the "@before" flag, this marks a function to be run before each test.
 *
 * This trait will trigger the usage of LDAP fixtures. To use, create a property in your test case like:
 * ```
 *      $this->ldap_fixtures = [
 *          \App\Test\Fixture\LdapUsersFixture::class, // Full namespace
 *          'app.LdapUsers', // Or use abstract namespace
 *      ];
 * ```
 * And let your class extends the LDAPI\TestSuite\Fixture\TestFixture class.
 *
 * @property string[] $ldap_fixtures    List of fixtures to be used
 */
trait LdapiTestTrait
{
    /**
     * Replace LDAPIConnection with a fixed one
     *
     * @before
     * @return void
     */
    public function setupConnections()
    {
        // Replace static LDAPI instance with a test one
        LDAPIConnection::setTestLdapi();

        // Trigger LDAP fixtures
        if (isset($this->ldap_fixtures) && is_array($this->ldap_fixtures))
        {
            foreach ($this->ldap_fixtures as $fixture_class)
            {
                // Catch non-literal namespace
                if (substr($fixture_class, 0, 4) == 'app.')
                {
                    $fixture_name = substr($fixture_class, 4);
                    $fixture_class = Configure::read('App.namespace') . "\\Test\\Fixture\\{$fixture_name}Fixture";
                }

                $fixture = new $fixture_class;
                $fixture->load(LDAPIConnection::get()); // Put fixture data into test storage
            }
        }
    }

    /**
     * Rests connection
     *
     * @after
     * @return void
     */
    public function cleanupLdapiTrait()
    {
        // Clear old fixtures

        TestLdapi::$_users = [];
        TestLdapi::$_committees = [];
        TestLdapi::$_forwards = [];
    }
}
