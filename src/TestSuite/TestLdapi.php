<?php

/**
 * @noinspection PhpMissingParentConstructorInspection
 */

namespace LdapiHelper\TestSuite;

use LDAPI\LDAPI;
use LDAPI\LDAPUser;
use LDAPI\LDAPCommittee;
use LDAPI\LDAPObject;

/**
 * Test extension of LDAPI
 *
 * Relies on local fixtures instead of an actual connection.
 * For this test version, no LDAP server is needed.
 *
 * The basic connect and get functions are overloaded, such that the LDAPI and
 * the returned objects still behave regularly.
 */
class TestLdapi extends LDAPI
{

    /**
     * Storages for fixtured object
     *
     * These properties should be treated like being static, let only the designated fixtures use them.
     *
     * @var LDAPUser[]      $_users      Users fixture data (keyed by id)
     * @var LDAPCommittee[] $_committees Committees fixture data (keyed by id)
     * @var string[]        $_forwards   Email forwards fixture data (keyed by user id)
     */
    static $_users = [], $_committees = [], $_forwards = [];

    /**
     * Overloaded constructor
     *
     * No actual configuration is loaded.
     *
     * {@inheritDoc}
     */
    public function __construct()
    {
        // Set internal values to fixed value just to make sure
        $this->config = false;
        $this->bind = false;
        $this->connection = false;

        // Potential test fixtures are not loaded yet - Those can be inserted later into the static lists of this class
    }

    /**
     * Overloaded destructor
     *
     * {@inheritDoc}
     */
    public function __destruct()
    {
        // Nothing to clean up
    }

    /**
     * Overloaded connection function - No actual connection is made
     *
     * {@inheritDoc}
     */
    protected function _makeConnection()
    {
        trigger_error('_makeConnection called, should never happen in test LDAPI!', E_USER_ERROR);
    }

    /**
     * Mocked find-objects base method
     *
     * {@inheritDoc}
     */
    protected function _findObjectsByValues($class, $criteria = null, $by_id = false)
    {
        if (self::classes_equal($class, LDAPUser::class))
        {
            $list = &self::$_users;
        }
        else if (self::classes_equal($class, LDAPCommittee::class))
        {
            $list = &self::$_committees;
        }
        else
        {
            $check = self::classes_equal($class, LDAPUser::class);
            trigger_error("Class `{$class}` not implemented!", E_USER_ERROR);
        }

        if ($criteria)
        {
            $results = array_filter($list, function ($var) use ($criteria)
            {
                return $this->_filterObjectByValues($var, $criteria);
            });
        }
        else
        {
            $results = $list;
        }

        if ($by_id)
        {
            return $results;
        }

        return array_values($results);
    }

    /**
     * Mocked filter method to suit the fixtures
     *
     * This does not yet perfectly mock the real LDAP filtering!
     *
     * {@inheritDoc}
     */
    protected function _filterObjectByValues($object, $criteria)
    {
        $found = true;

        foreach ($criteria as $key => $value) // Base level (AND)
        {
            if (is_array($value)) // List of criteria: OR
            {
                $sub_found = false;
                foreach ($value as $key_child => $value_child)
                {
                    $search_key = is_numeric($key_child) ? $key : $key_child;

                    if (isset($object->{$search_key}) &&
                        self::fields_equal_wildcards($value_child, $object->{$search_key}))
                    {
                        $sub_found = true; // At least one entry was found
                    }
                }

                if (!$sub_found)
                {
                    $found = false;
                }
            }
            else // Single key-value pair
            {
                if (isset($object->{$key}))
                {
                    if (is_array($object->{$key})) //  && !in_array($value, $object->{$key})
                    {
                        $in_array = false; // Do a manual in_array check to include wildcard search
                        foreach ($object->{$key} as $object_value)
                        {
                            if (self::fields_equal_wildcards($value, $object_value))
                            {
                                $in_array = true;
                                break;
                            }
                        }
                        if (!$in_array)
                        {
                            $found = false;
                        }
                    }
                    else
                    {
                         if (!self::fields_equal_wildcards($value, $object->{$key}))
                         {
                             $found = false;
                         }
                    }
                }
            }
        }

        return $found;
    }

    /**
     * Login user from fixtures
     *
     * Passwords are not saved with users. So instead for testing
     * the username is used as the password.
     *
     * {@inheritDoc}
     */
    public function login($username, $password)
    {
        $user = $this->getUserByUsername($username);

        if (!$user)
        {
            return false; // User could not be found
        }

        if ($user->username != $password)
        {
            return false;
        }

        return $user;
    }

    /**
     * Change the users password
     *
     * Result is not actually saved! There is no place yet to fix
     * user passwords.
     *
     * {@inheritDoc}
     */
    public function _changeUserPassword($user, $newPassword)
    {
        return true; // Nothing to be done, always succesful
    }

    /**
     * Save an object by updating the registered fixtures
     *
     * {@inheritDoc}
     */
    public function _saveNewObject($object, $additional_fields = null)
    {
        // Validate user object, all fields
        if ($object->_validate($object::LDAP_KEYS) === false)
            return false;

        $object->patchEntity([
            'id' => $this->_getNextObjectID(get_class($object))
        ]);

        $object->_clearDirty();

        if ($object instanceof LDAPUser)
        {
            $list = &self::$_users;
        }
        else
        {
            $list = &self::$_committees;
        }

        $list[$object->id] = $object;

        return true; // Save will always be successful
    }

    /**
     * Update fixtures with modified object
     *
     * {@inheritDoc}
     */
    public function _saveUpdatedObject($object)
    {
        // Validate user object
        if ($object->_validate() === false)
            return false;

        if ($object instanceof LDAPUser)
        {
            $list = &self::$_users;
        }
        else
        {
            $list = &self::$_committees;
        }

        if (!isset($list[$object->id]))
        {
            return false; // Object does not exist in fixtures
        }

        $object->_clearDirty();

        $list[$object->id] = $object; // Replace object

        return true; // Successful
    }

    /**
     * Returned fixtured email forward
     *
     * {@inheritDoc}
     */
    public function _getEmailForward($user)
    {
        if (isset(self::$_forwards[$user->id]))
        {
            return self::$_forwards[$user->id];
        }

        return false; // Nothing found
    }

    /**
     * Update fixtured email forward
     *
     * {@inheritDoc}
     */
    public function _saveEmailForward($user, $email)
    {
        // Check email
        if ($email !== false && !filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            $user->_setErrors('emailforward', 'Email is invalid');
            return false;
        }

        self::$_forwards[$user->id] = $email;
        return true;
    }

    /**
     * Remove a fixtured committee
     *
     * {@inheritDoc}
     */
    public function deleteCommittee($committee)
    {
        if (!isset(self::$_committees[$committee->id]))
        {
            return false; // Doesn't exist
        }

        unset(self::$_committees[$committee->id]);

        return true;
    }

    /**
     * {@inheritDoc}}
     */
    public function deleteUser($user)
    {
        if (!isset(self::$_users[$user->id]))
        {
            return false; // User does not exist
        }

        $user->collectCommittees(); // Get committees before user is deleted

        // Remove user from all committees
        foreach ($user->committees as $committee)
        {
            $committee->removeMembers($user);
        }

        unset(self::$_users[$user->id]);

        return true;
    }

    /**
     * Enable write check by default
     *
     * {@inheritDoc}
     */
    public function _writeCheck()
    {
        return true;
    }

    /**
     * Extension of is_subclass_of()
     *
     * Checks if $a is a subclass of $b, or equal to $b.
     * Both parameters are meant to be class names, not objects.
     *
     * @param string $a
     * @param string $b
     * @return bool
     */
    public static function classes_equal($a, $b)
    {
        if ($a == $b)
        {
            return true;
        }

        return is_subclass_of($a, $b);
    }

    /**
     * Mimic field evaluation in LDAP, can process the whildcards in a search.
     *
     * '*' is used for any number of characters (including none) and '?'
     * for exactly one character.
     *
     * @param $search
     * @param $subject
     */
    public static function fields_equal_wildcards($search, $subject)
    {
        if (!is_string($search))
        {
            return $search == $subject; // Do simple (non type strict) comparison for non-string
        }

        // Convert wildcard to regex
        $search_re = strtr($search, [
                '*' => '.*?', // 0 or more (lazy) - asterisk (*)
                '?' => '.', // 1 character - question mark (?)
        ]);
        return preg_match("/$search_re/", $subject);
    }

}
