<?php

namespace LdapiHelper\TestSuite;

/**
 * Alias, use LdapiTestTrait instead
 *
 * @deprecatd The test trait has been renamed to a more proper name. Use the `LdapiTestTrait` instead.
 * @see LdapiTestTrait
 * @package LdapiHelper\TestSuite
 */
trait LdapiTrait
{
    use LdapiTestTrait {
        LdapiTestTrait::setupConnections as parentSetupConnections;
    }

    /**
     * Replace LDAPIConnection with a fixed one
     *
     * @before
     * @return void
     */
    public function setupConnections()
    {
        trigger_error('The test trait has been renamed to a more proper name. Use the `LdapiTestTrait` instead.', E_USER_DEPRECATED);

        $this->parentSetupConnections();
    }

}
