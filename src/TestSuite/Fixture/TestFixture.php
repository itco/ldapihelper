<?php

namespace LdapiHelper\TestSuite\Fixture;

use LDAPI\LDAPObject;
use LdapiHelper\TestSuite\TestLdapi;

/**
 * LdapiHelper fixtures
 *
 * Fixture classes for users and committees will extend from this one. The main
 * goal of this class is to turn arrays into LDAP objects.
 *
 * @property TestLdapi $ldapi_link Link to test LDAPI
 */
abstract class TestFixture
{

    /**
     * @var array $records
     * @var array $base_record Base record, will be merged with each record to save a user a lot of typing
     */
    public $records, $base_record;

    /**
     * @var string|null Use the constants below to specify which type your fixture will contain
     */
    public $type = null;

    /**
     * The possible types of fixtures
     */
    const TYPE_USER = 'user';
    const TYPE_COMMITTEE = 'committee';
    const TYPE_FORWARD = 'forward';

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->records = [];
        $this->base_record = [];

        $this->init();
    }

    /**
     * Init
     *
     * Let user overload to fill in records (and base_record)
     */
    abstract public function init();

    /**
     * Turn records into LDAP entities
     *
     * @param TestLdapi $ldapi LDAPI instance (needed to create objects)
     * @return LDAPObject[]
     */
    protected function getObjects($ldapi)
    {
        $objects = [];

        foreach ($this->records as $record)
        {
            if ($this->base_record)
            {
                // Extend the base_record if it is set
                $record = array_merge($this->base_record, $record);
            }

            if ($this->type == self::TYPE_USER)
            {
                $object = $ldapi->createUserWithLink($record);
                $id = $object->id;
            }
            elseif ($this->type == self::TYPE_COMMITTEE)
            {
                $object = $ldapi->createCommitteeWithLink($record);
                if ($record['member_usernames']) // Member usernames are not carried over correctly
                {
                    $object->member_usernames = $record['member_usernames'];
                }
                $id = $object->id;
            }
            elseif ($this->type == self::TYPE_FORWARD)
            {
                $object = $record['email'];
                $id = $record['user_id'];
            }
            else
            {
                trigger_error("Error with LDAP fixtures - Unknown fixture type `{$this->type}`", E_USER_ERROR);
            }

            if (is_object($object))
            {
                if ($object->getErrors())
                {
                    print_r($object);
                    print_r($object->getErrors());
                    trigger_error("Error with LDAP fixture record with type `{$this->type}`", E_USER_ERROR);
                }
                else
                {
                    $object->_clearDirty();
                }
            }

            $objects[$id] = $object; // Store each entity by its id
        }

        return $objects;
    }

    /**
     * Load this fixture into the LDAP test storage
     *
     * @param TestLdapi $ldapi
     */
    public function load($ldapi)
    {
        $objects = $this->getObjects($ldapi);

        if ($this->type == self::TYPE_USER)
        {
            TestLdapi::$_users = $objects;
        }
        elseif ($this->type == self::TYPE_COMMITTEE)
        {
            TestLdapi::$_committees = $objects;
        }
        elseif ($this->type == self::TYPE_FORWARD)
        {
            TestLdapi::$_forwards = $objects;
        }
    }

}
