<?php

namespace LdapiHelper\LDAP;

use LDAPI\LDAPI;
use LdapiHelper\TestSuite\TestLdapi;

/**
 * Static wrapper to share a single LDAPI instance
 */
class LDAPIConnection
{
    private static $_ldapi = null;
    
    /**
     * Get LDAP reference
     * 
     * @return LDAPI
     */
    public static function get()
    {
        if (!self::$_ldapi)
        {
            self::$_ldapi = new LDAPI();
        }
        
        return self::$_ldapi;
    }
    
    /**
     * Set local LDAPI instance to a test version
     * 
     * @return void
     */
    public static function setTestLdapi()
    {
        self::$_ldapi = new TestLdapi();
    }
}
