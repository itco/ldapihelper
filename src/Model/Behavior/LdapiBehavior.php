<?php

namespace LdapiHelper\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Query;
use Cake\Collection\CollectionInterface;
use LdapiHelper\LDAP\LDAPIConnection;

/**
 * LdapiBehavior behavior
 *
 * Possible options:
 *   - `fields.user` => Column containing LDAPUser id (default: 'user_id')
 *   - `fields.committee` => Column containing LDAPCommittee id (default: 'committee_id')
 *   - `entities.user` => Key used to store LDAPUser (default: 'user')
 *   - `entities.committee` => Key used to store LDAPCommittee (default: 'committee')
 */
class LdapiBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'fields' => [
            'user' => 'user_id',
            'committee' => 'committee_id'
        ],
        'entities' => [
            'user' => 'user',
            'committee' => 'committee'
        ]
    ];

    /**
     * The field used during *the current query*
     *
     * This is a property so it is easily shared amongst embedded methods.
     * 
     * @var string
     */
    private $_association = false;

    /**
     * Include LDAPCommittee objects through LDAPI
     *
     * This makes use of the user and entity config options.
     * 
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findWithCommittees(Query $query, array $options)
    {
        $this->_association = 'committee';
        return $query->formatResults([$this, 'formatterObjects']);
    }
    
    /**
     * Include LDAPUser objects through LDAPI
     *
     * This makes use of the user and entity config options.
     * 
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findWithUsers(Query $query, array $options)
    {
        $this->_association = 'user';
        return $query->formatResults([$this, 'formatterObjects']);
    }

    /**
     * Takes in the results collection of a ORM query and process the results
     * 
     * Returns a modified collection, in this case included with LDAPI objects.
     * 
     * @param CollectionInterface $results
     * @return CollectionInterface
     */
    public function formatterObjects(CollectionInterface $results)
    {
        $association = $this->_association;
        $field = $this->_config['fields'][$association];

        $ids = array_unique($results->extract($field)->toArray());

        switch ($association)
        {
            case 'committee':
                $objectsMap = LDAPIConnection::get()->getCommittees($ids);
                break;
            case 'user':
                $objectsMap = LDAPIConnection::get()->getUsers($ids);
                break;
            default:
                trigger_error("Type `{$association}` is not implemented", E_USER_ERROR);
        }

        $entity_name = $this->_config['entities'][$association];

        return $results->map(function ($entity) use ($objectsMap, $entity_name, $field)
                {
                    if (isset($entity[$field]) && isset($objectsMap[$entity[$field]]))
                    {
                        $entity[$entity_name] = $objectsMap[$entity[$field]];
                    }

                    return $entity;
                });
    }

}
