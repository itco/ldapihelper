<?php

namespace LdapiHelper;

use Cake\Core\BasePlugin;

/**
 * Plugin for LdapiHelper
 *
 * This class is only a stub so it can be easily found through namespace.
 */
class Plugin extends BasePlugin
{
    
}
