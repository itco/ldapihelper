API documentation
=================

Plugin
------

.. doxygenclass:: LdapiHelper::Plugin


LDAPIConnection
---------------

.. doxygenclass:: LdapiHelper::LDAP::LDAPIConnection


LdapiBehavior
-------------

.. doxygenclass:: LdapiHelper::Model::Behavior::LdapiBehavior


LdapiTrait
----------

(Auto doc not supported for PHP Traits.)

.. class:: LdapiHelper::TestSuite::LdapiTestTrait

   Include this trait in your test case to fake calls to the LDAP:

   `use LdapiTestTrait;`

   Instead of trying to use a remote connection, the local fixture files are used.


TestLdapi
---------

.. doxygenclass:: LdapiHelper::TestSuite::TestLdapi


TestFixture
-----------

.. doxygenclass:: LdapiHelper::TestSuite::Fixture::TestFixture
