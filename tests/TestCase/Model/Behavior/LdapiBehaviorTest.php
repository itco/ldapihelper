<?php
namespace LdapiHelper\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use LDAPI\LDAPCommittee;
use LDAPI\LDAPUser;
use LdapiHelper\Model\Behavior\LdapiBehavior;
use Cake\ORM\Table;
use Cake\TestSuite\IntegrationTestTrait;
use LdapiHelper\TestSuite\LdapiTestTrait;
use LdapiHelper\Test\Fixture\ExamplesFixture;
use LdapiHelper\Test\Fixture\LdapUsersFixture;
use LdapiHelper\Test\Fixture\LdapCommitteesFixture;

/**
 * LdapiHelper\Model\Behavior\LdapiBehavior Test Case
 */
class LdapiBehaviorTest extends TestCase
{

    use IntegrationTestTrait;
    use LdapiTestTrait;

    /**
     * CakePHP test fixtures to load
     *
     * @var string[]
     */
    public $fixtures = [
        ExamplesFixture::class
    ];

    public $ldap_fixtures = [
        LdapUsersFixture::class,
        LdapCommitteesFixture::class
    ];

    /**
     * Table to suite test subject
     *
     * @var Table
     */
    public $table;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->table = new Table([
            'table' => 'examples',
            'alias' => 'Examples'
        ]);

        $this->table->addBehavior(LdapiBehavior::class, [
            'fields' => [ // These match the default, so they aren't strictly necessary
                'user' => 'user_id',
                'committee' => 'committee_id'
            ],
            'entities' => [
                'user' => 'ldap_user',
                'committee' => 'ldap_committee'
            ]
        ]);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->table);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->assertTrue(
            $this->table->hasBehavior(LdapiBehavior::class)
        );
    }

    /**
     * Test initial setup
     *
     * @see LdapiBehavior::findWithUsers()
     */
    public function testFindUsers()
    {
        $query = $this->table->find('withUsers');

        $entries = $query->toArray();

        $this->assertGreaterThanOrEqual(2, count($entries));

        foreach ($entries as $entry)
        {
            $this->assertNotNull($entry->ldap_user);
            $this->assertInstanceOf(LDAPUser::class, $entry->ldap_user);
            $this->assertEquals($entry->user_id, $entry->ldap_user->id);
        }
    }

    /**
     * Test initial setup
     *
     * @see LdapiBehavior::findWithCommittees()
     */
    public function testFindCommittees()
    {
        $query = $this->table->find('withCommittees');

        $entries = $query->toArray();

        $this->assertGreaterThanOrEqual(2, count($entries));

        foreach ($entries as $entry)
        {
            $this->assertNotNull($entry->ldap_committee);
            $this->assertInstanceOf(LDAPCommittee::class, $entry->ldap_committee);
            $this->assertEquals($entry->committee_id, $entry->ldap_committee->id);
        }
    }
}
