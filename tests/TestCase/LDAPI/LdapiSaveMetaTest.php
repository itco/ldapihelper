<?php

namespace LDAPI;

use LdapiHelper\TestSuite\LdapiTestTrait;
use PHPUnit\Framework\TestCase;
use LdapiHelper\LDAP\LDAPIConnection;

/**
 * Test the 'save' functionalities of the mock-LDAPI
 */
class LdapiSaveMetaTest extends TestCase
{

    // Refer to mock LDAPI
    use LdapiTestTrait;

    const USER_DATA = [
        'address' => 'Groove Street 69',
        'attentie' => 0,
        'authorization' => 4,
        'bic' => 'INGBNL2A',
        'birthdate' => '1990-01-01',
        'city' => 'Vice City',
        'class' => 15,
        'country' => 'nl',
        'email' => 'voornaam.achternaam50@gmail.com',
        'emailaliases' => ['voornaam.achternaam'],
        'firstname' => 'Voornaam',
        'iban' => 'NL57DNIB0400333821',
        //'id' => null,
        'initials' => 'V.',
        'insertions' => null,
        'joindate' => '2019-10-13',
        'lastname' => 'Achternaam',
        'membership' => 0,
        'names' => 'Voornaam',
        'nationality' => 'Dutch',
        'phonenumber' => '0612345678',
        'phonenumber_home' => '0201234567',
        'postalcode' => '6969AA',
        'studentnumber' => '1599999',
        'username' => 'voornaam.achternaam'
    ];

    /**
     * Test creating a new committee
     */
    public function testAddCommittee()
    {
        $data = ['name' => 'MyCoolCommittee'];
        $committee1 = LDAPIConnection::get()->createCommitteeWithLink($data);
        $result = LDAPIConnection::get()->saveNewCommittee($committee1);

        $this->assertTrue($result);

        $committee2 = LDAPIConnection::get()->getCommitteeByName($data['name']);
        $this->assertNotFalse($committee2);
        $this->assertEquals($committee1->name, $committee2->name);

        // And delete it

        $result = LDAPIConnection::get()->deleteCommittee($committee2);

        $this->assertTrue($result);

        $committee3 = LDAPIConnection::get()->getCommittee($committee2->id);

        $this->assertFalse($committee3);
    }

    /**
     * Test creating a new user and patching it again
     */
    public function testAddUser()
    {
        $data = self::USER_DATA;
        $user1 = LDAPIConnection::get()->createUserWithLink($data);
        $result = LDAPIConnection::get()->saveNewUser($user1, 'coolpassword');

        $this->assertTrue($result);

        $user2 = LDAPIConnection::get()->getUser($user1->id);
        $this->assertNotFalse($user2);
        $this->assertEquals($user1->username, $user2->username);

        // Now modify it again

        $data = ['city' => 'Coolplace McAwesomeville'];
        $user2->patchEntity($data);
        $result = LDAPIConnection::get()->saveUpdatedUser($user2);

        $this->assertTrue($result);

        $user3 = LDAPIConnection::get()->getUser($user1->id);
        $this->assertEquals($data['city'], $user3->city);

        // And finally delete it

        $result = LDAPIConnection::get()->deleteUser($user3);

        $this->assertTrue($result);

        $user4 = LDAPIConnection::get()->getUser($user1->id);

        $this->assertFalse($user4);
    }

}