<?php

namespace LdapiHelper\Test\TestCase\LDAPI;

use LdapiHelper\TestSuite\LdapiTrait;
use PHPUnit\Framework\TestCase;
use LdapiHelper\LDAP\LDAPIConnection;
use LdapiHelper\TestSuite\LdapiTestTrait;
use LdapiHelper\Test\Fixture\LdapUsersFixture;
use LdapiHelper\Test\Fixture\LdapCommitteesFixture;
use LdapiHelper\Test\Fixture\LdapForwardsFixture;

/**
 * This test case test the test version of the LDAPI
 *
 * Pretty meta. This is mostly just an example of how the LDAPI would be used in tests.
 */
class LdapiFixturesMetaTest extends TestCase
{

    use LdapiTestTrait; // Include the test trait to turn off real LDAP connection

    public $ldap_fixtures = [
        LdapUsersFixture::class,
        LdapCommitteesFixture::class,
        LdapForwardsFixture::class
    ];

    /**
     * Make use of a Users fixture
     */
    public function testUsers()
    {
        // If this does not throw an error, we're golden
        $user = LDAPIConnection::get()->getUser(3500);
        $this->assertNotFalse($user);
        $this->assertEquals(3500, $user->id);
    }

    /**
     * Make use of a Committees fixture
     */
    public function testCommittees()
    {
        // If this does not throw an error, we're golden
        $committee = LDAPIConnection::get()->getCommittee(65535);
        $this->assertNotFalse($committee);
        $this->assertEquals(65535, $committee->id);
    }

    /**
     * Make use of a Forwards fixture
     */
    public function testForwards()
    {
        // If this does not throw an error, we're golden
        $user = LDAPIConnection::get()->getUser(3500);
        $email = $user->getEmailForward();
        $this->assertStringEndsWith('@gmail.com', $email);
    }
}
