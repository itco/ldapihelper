<?php

namespace LdapiHelper\Test\TestCase\LDAPI;

use PHPUnit\Framework\TestCase;
use LdapiHelper\LDAP\LDAPIConnection;
use LdapiHelper\TestSuite\LdapiTestTrait;
use LdapiHelper\TestSuite\TestLdapi;

/**
 * This test case test the test version of the LDAPI
 *
 * Pretty meta. This is mostly just an example of how the LDAPI would be used in tests.
 */
class LdapiMetaTest extends TestCase
{

    use LdapiTestTrait; // Include the test trait to turn of real LDAP connection

    /**
     * Test whether the connection helper really returns a test version
     */
    public function testTestLdapi()
    {
        $ldap = LDAPIConnection::get();
        $this->assertInstanceOf(TestLdapi::class, $ldap);
    }
}
