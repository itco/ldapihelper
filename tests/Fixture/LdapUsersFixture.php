<?php

namespace LdapiHelper\Test\Fixture;

use LdapiHelper\TestSuite\Fixture\TestFixture;

/**
 * Example of a Users fixture
 *
 * Do not use this class directly in your own fixtures! The purpose of this specific class is to test the fixture
 * loading.
 */
class LdapUsersFixture extends TestFixture
{

    /**
     * Test object class of this fixture
     */
    public $type = TestFixture::TYPE_USER;

    /**
     * Load records
     */
    public function init()
    {
        // Basic information to save a lot of typing
        $this->base_record = [
            'address' => 'Groove Street 69',
            'attentie' => 0,
            'authorization' => 4,
            'bic' => 'INGBNL2A',
            'birthdate' => '1990-01-01',
            'city' => 'Vice City',
            'class' => 15,
            'country' => 'nl',
            'email' => 'voornaam.achternaam50@gmail.com',
            'emailaliases' => ['voornaam.achternaam'],
            'firstname' => 'Voornaam',
            'iban' => 'NL57DNIB0400333821',
            //'id' => null,
            'initials' => 'V.',
            'insertions' => null,
            'joindate' => '2019-10-13',
            'lastname' => 'Achternaam',
            'membership' => 0,
            'names' => 'Voornaam',
            'nationality' => 'Dutch',
            'phonenumber' => '0612345678',
            'phonenumber_home' => '0201234567',
            'postalcode' => '6969AA',
            'studentnumber' => '1599999',
            'username' => 'voornaam.achternaam'
        ];

        $this->records = [
            [
                'username' => 'piet.tester',
                'id' => 3500,
                'studentnumber' => '1512345'
            ],
            [
                'username' => 'robert.roos',
                'firstname' => 'Robert',
                'lastname' => 'Roos',
                'id' => 3695,
                'studentnumber' => '1589482',
                'initials' => 'R.A.'
            ],
            [
                'username' => 'voornaam.achternaam',
                'id' => 3503,
                'studentnumber' => '1589600',
                'initials' => 'V.'
            ],
        ];
    }
}
