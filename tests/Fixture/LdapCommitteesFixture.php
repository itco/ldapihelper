<?php

namespace LdapiHelper\Test\Fixture;

use LdapiHelper\TestSuite\Fixture\TestFixture;

/**
 * Example of a Committees fixture
 *
 * Do not use this class directly in your own fixtures! The purpose of this specific class is to test the fixture
 * loading.
 */
class LdapCommitteesFixture extends TestFixture
{

    /**
     * Test object class of this fixture
     */
    public $type = TestFixture::TYPE_COMMITTEE;

    /**
     * Load records
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 65535,
                'name' => 'ITco',
                'member_usernames' => [
                    'robert.roos',
                    'kasper.hendriks',
                    'piet.tester'
                ]
            ],
            [
                'id' => 65552,
                'name' => 'SympoCie',
                'member_usernames' => [
                    'robert.roos',
                    'voornaam.achternaam'
                ]
            ],
            [
                'id' => 65569,
                'name' => 'BOSS',
                'member_usernames' => [
                    'voornaam.achternaam'
                ]
            ]
        ];
    }
}
