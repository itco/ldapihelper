<?php

namespace LdapiHelper\Test\Fixture;

use LdapiHelper\TestSuite\Fixture\TestFixture;

/**
 * Example of a email forwards fixture
 *
 * Do not use this class directly in your own fixtures! The purpose of this specific class is to test the fixture
 * loading.
 */
class LdapForwardsFixture extends TestFixture
{

    /**
     * Test object class of this fixture
     */
    public $type = TestFixture::TYPE_FORWARD;

    /**
     * Load records
     */
    public function init()
    {
        $this->records = [
            [
                'email' => 'piet.tester@gmail.com',
                'user_id' => 3500,
            ],
        ];
    }
}
