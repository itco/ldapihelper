<?php

declare(strict_types=1);

namespace LdapiHelper\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * HostsFixture
 */
class ExamplesFixture extends TestFixture
{

    /**
     * Test table structure
     *
     * @var array
     */
    public $fields = [
        'id' => ['type' => 'integer'],
        'user_id' => ['type' => 'integer', 'null' => false],
        'committee_id' => ['type' => 'integer', 'null' => false],
        'created' => 'datetime',
        'modified' => 'datetime',
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id']]
        ]
    ];

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'user_id' => 3500,
                'committee_id' => 65535,
                'created' => '2020-10-25 10:36:44',
                'modified' => '2020-10-25 10:36:44',
            ],
            2 => [
                'user_id' => 3695,
                'committee_id' => 65552,
                'created' => '2020-10-25 10:36:44',
                'modified' => '2020-10-25 10:36:44',
            ],
        ];
        parent::init();
    }

}
