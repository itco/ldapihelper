# LdapiHelper plugin for CakePHP

[![CodeFactor](https://www.codefactor.io/repository/bitbucket/itco/ldapihelper/badge)](https://www.codefactor.io/repository/bitbucket/itco/ldapihelper)
[![codecov](https://codecov.io/bb/itco/ldapihelper/branch/master/graph/badge.svg)](https://codecov.io/bb/itco/ldapihelper)
[![Documentation Status](https://readthedocs.org/projects/ldapihelper/badge/?version=latest)](https://ldapihelper.readthedocs.io/en/latest/?badge=latest)

## Installation

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

Run
```
composer require itco/LdapiHelper
```

When using the LdapiHelper, do *not* explicitly require the LDAPI package too.

## Usage

See the repository wiki for more information: <https://bitbucket.org/itco/ldapihelper/wiki/Home>

## Documentation

Documentation is automatically built and hosted by ReadTheDocs: <https://ldapihelper.readthedocs.io/>

This documentation is built through Doxygen and Sphinx (Python package), with the Breathe extension. Build it like:

```shell
cd docs
make html
```

Run `pip install -r docs/requirements.txt` to get the required packages.  
The build output can be found in `docs/_build/html`.

### PHPDoc ###

An easier way to build documentation is using [phpDocumentor](https://www.phpdoc.org/). Generated PHPDoc documentation can be found here: https://itco.bitbucket.io/ldapihelper/index.html (note: this is *not* build automatically and can be out-of-date).

Generate documentation with:

```shell
phpdoc -d ./src -t ./phpdoc_build
```

Use `-d` to set the input and `-t` to set the output.

## Changelog ##

See [CHANGELOG.md](CHANGELOG.md) for an overview.
